// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDRDOOu_0Lp6dULad7Uv1BveOWdvCSChiM",
    authDomain: "bogorku-635f9.firebaseapp.com",
    projectId: "bogorku-635f9",
    storageBucket: "bogorku-635f9.appspot.com",
    messagingSenderId: "1054309368561",
    appId: "1:1054309368561:web:1b3e67d240966c17459f50"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
