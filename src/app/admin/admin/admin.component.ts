import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  constructor(
    public router: Router,
    public auth: AngularFireAuth
    
  ) { }

  ngOnInit(): void {
  }

  mode:string='side';

  logout()
  {
    let conf=confirm('Keluar aplikasi?');
    if(conf)
    {
      this.auth.signOut().then(res=>{
        this.router.navigate(['auth/login']);
      }).catch(err=>{
        alert('Logout Gagal')
      });
    }
  }
}
