import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HargaDetailComponent } from './harga-detail.component';

describe('HargaDetailComponent', () => {
  let component: HargaDetailComponent;
  let fixture: ComponentFixture<HargaDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HargaDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HargaDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
