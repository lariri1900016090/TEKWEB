import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  title:any;
  book:any={};
  constructor() { }

  ngOnInit(): void {
    this.title='Profil';
    this.book={
      name:'Susi',
      date:'19-02-2001',
      age:20,
      address:'jln ki ageng pemanahan',
    };
  }
  

}
