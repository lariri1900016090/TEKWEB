import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin/admin.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RouterModule, Routes } from '@angular/router';
import { MaterialDesign } from '../material/material';
import { ImagesComponent } from './images/images.component';
import { ProductComponent } from './product/product.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { FormsModule } from '@angular/forms';
import { HargaDetailComponent } from './harga-detail/harga-detail.component';
import { PriceComponent } from './price/price.component';


const routes: Routes = [
{
	path:'',
	component:AdminComponent,
	children:[
	{
		path:'product',
		component:ProductComponent
	},
	{
		path:'price',
		component:PriceComponent
	},
	{
		path:'dashboard',
		component:DashboardComponent
	},
	{
		path:'images',
		component:ImagesComponent
	},
	{
		path:'',
		pathMatch:'full',
		redirectTo:'/admin/dashboard'
	}
	]
},
]

@NgModule({
  declarations: [AdminComponent, DashboardComponent, ImagesComponent, ProductComponent, ProductDetailComponent,HargaDetailComponent, PriceComponent,],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
	MaterialDesign,
	FormsModule
	
  ],
})
export class AdminModule { }
