import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})

export class RegisterComponent implements OnInit {
  user:any= {};
  constructor(
    public router: Router,
    public auth: AngularFireAuth
  ) { }

  ngOnInit(): void {
  }

  hide: boolean =true;

  //form validation
    name= new FormControl('',[Validators.required]);
    email= new FormControl('',[Validators.required, Validators.email]);
    password= new FormControl('',[Validators.minLength(6), Validators.required]);
    confirmPassword= new FormControl('',[Validators.minLength(6), Validators.required]);

    //register
    User:any ;
    loading!:boolean;
    register()
    {
      this.loading=true;
      this.auth.createUserWithEmailAndPassword(this.user.email, this.user.password).then(res=>{
        this.loading=false;
        alert('Registrasi Berhasil');
        this.router.navigate(['/Login']);
      }).catch(err=>{
        this.loading=false;
        alert('Ada masalah');
      });
    }
  }