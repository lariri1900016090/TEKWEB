import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  user: any = {};
  constructor(
    public router: Router,
    public auth: AngularFireAuth
  ) { }

  ngOnInit(): void {
  }

  login()
  {
    this.auth.signInWithEmailAndPassword(this.user.email, this.user.password).then(res=>{
      this.router.navigate(['admin/dashboard']);
    }).catch(err=>{
      alert('Tidak dapat login');
    })
  }
}
